FROM php:8.1-fpm AS app-php

RUN apt-get update && apt-get upgrade -y && apt-get install -y \
    git \
    nano \
    libxml2-dev \
    sudo \
    zip \
    unzip \
    && docker-php-ext-install intl opcache

COPY ./docker/php/php.ini /usr/local/etc/php/php.ini

RUN pecl install xdebug && docker-php-ext-enable xdebug

COPY ./docker/php/xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
RUN chmod +x /usr/bin/composer
ENV COMPOSER_ALLOW_SUPERUSER 1

#RUN curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.deb.sh' | sudo -E bash
#RUN apt-get install symfony-cli

#WORKDIR /var/www/html

#CMD symfony server:start

FROM nginx:1.23.1 AS app-nginx

RUN rm -rf /etc/nginx/conf.d/*

COPY ./docker/nginx/default.conf /etc/nginx/conf.d/default.conf